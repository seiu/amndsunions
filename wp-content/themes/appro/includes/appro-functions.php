<?php
function appro_body_classes( $classes ) {
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}
	return $classes;
}
add_filter( 'body_class', 'appro_body_classes' );
/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function appro_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'appro_pingback_header' );
/*-- Mainmenu-Area --*/
if( ! function_exists('appro_main_menu_content') ){
    function appro_main_menu_content( $tr_check = '' ){ 
        /*-- Main Menu Area --*/
        $sticky_menu = get_theme_mod('sticky_nav_menu');
        $transparent_main_menu = get_theme_mod('transparent_main_menu');
        
        if( $tr_check == 'yes' ){
            $tr_check = 'tr';
        }elseif( $transparent_main_menu ){
            $tr_check = 'tr';
        }else{
            $tr_check = '';
        }

?><nav class="mainmenu-area <?php echo esc_attr($tr_check); ?>" <?php if($sticky_menu){ echo 'data-spy="affix" data-offset-top="200"'; } ?> >
            <div class="container">
                <div class="navbar-header ">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mainmenu">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span> 
                    </button>
                    <?php if( has_custom_logo() ): ?>
                        <?php the_custom_logo(); ?>
                    <?php else: ?>
                        <a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>"><?php bloginfo( 'name' ); ?></a>
                    <?php endif; ?>
                </div>
                <div class="collapse navbar-collapse navbar-right" id="mainmenu">
                    <?php
                        if( function_exists('wp_nav_menu') ){
                            wp_nav_menu(array(
                                'theme_location'    => 'mainmenu',
                                'container'         => false,
                                'menu_class'        => 'nav navbar-nav',
                                'fallback_cb'       => 'appro_mainmenu_demo_content',
                                'walker'            =>  new appro_Nav_Menu_Walker
                            ));
                        }
                ?></div>
            </div>
        </nav>
    <?php }    
}
if( !function_exists('appro_preloader_content')){
    function appro_preloader_content(){
        $preloader_switch = get_theme_mod('appro_preloader_switch');
        if( $preloader_switch ){
            return;   
        }
?>
<div class="preloade">
    <span><i class="fa fa-clock-o"></i></span>
</div>
<?php }
}
/****************
Social Menu Link
****************/
if( ! function_exists('appro_social_menu_link') ){    
    function appro_social_menu_link(){
        $facebook = get_theme_mod( 'facebook' );
        $twitter = get_theme_mod( 'twitter' );
        $linkedin = get_theme_mod( 'linkedin' );
        $instagram = get_theme_mod( 'instagram' );
        $flickr = get_theme_mod( 'flickr' );
        $pinterest = get_theme_mod( 'pinterest' );
        $dribbble = get_theme_mod( 'dribbble' );
        $google_plus = get_theme_mod( 'google_plus' );
        $youtube = get_theme_mod( 'youtube' );
        if(!empty($facebook)){
            printf( wp_kses('<a target="_blank" href="%s"><i class="fa fa-facebook"></i></a>', wp_kses_allowed_html( 'post' )) , esc_url($facebook) );            
        }
        if(!empty($twitter)){            
            printf( wp_kses('<a target="_blank" href="%s"><i class="fa fa-twitter"></i></a>', wp_kses_allowed_html( 'post' )) , esc_url($twitter) );             
        }
        if(!empty($linkedin)){
            printf( wp_kses('<a target="_blank" href="%s"><i class="fa fa-linkedin"></i></a>', wp_kses_allowed_html( 'post' )) , esc_url($linkedin) );    
        }
        if(!empty($instagram)){
            printf( wp_kses('<a target="_blank" href="%s"><i class="fa fa-instagram"></i></a>', wp_kses_allowed_html( 'post' )) , esc_url($instagram) );    
        }
        if(!empty($flickr)){
            printf( wp_kses('<a target="_blank" href="%s"><i class="fa fa-flickr"></i></a>', wp_kses_allowed_html( 'post' )) , esc_url($flickr) );   
        }
        if(!empty($pinterest)){
            printf( wp_kses('<a target="_blank" href="%s"><i class="fa fa-pinterest"></i></a>', wp_kses_allowed_html( 'post' )) , esc_url($pinterest) );
        }
        if(!empty($dribbble)){
            printf( wp_kses('<a target="_blank" href="%s"><i class="fa fa-dribbble"></i></a>', wp_kses_allowed_html( 'post' )) , esc_url($dribbble) );
        }
        if(!empty($google_plus)){            
            printf( wp_kses('<a target="_blank" href="%s"><i class="fa fa-google-plus"></i></a>', wp_kses_allowed_html( 'post' )) , esc_url($google_plus) );
        }
        if(!empty($youtube)){
            printf( wp_kses('<a target="_blank" href="%s"><i class="fa fa-youtube"></i></a>', wp_kses_allowed_html( 'post' )) , esc_url($youtube) );
        }
    }
}
/*-- Mainmenu-Demo-Content --*/
if( !function_exists('appro_mainmenu_demo_content')){
    function appro_mainmenu_demo_content(){ 
        if(!current_user_can('edit_theme_options')){
            return;
        }
        printf( wp_kses('<ul class="nav navbar-nav"><li><a href="%s">%s</a></li></ul>',wp_kses_allowed_html('post')), esc_url(admin_url('nav-menus.php')), esc_html__('Create a new menu','appro'));
    }
}
/******************************
* Comment-Form
******************************/
if( ! function_exists('appro_comment_form_df') ){
    function appro_comment_form_df( $default ){
        global $current_user;
        $default['comment_field'] = '<div class="form-group"><textarea class="form-control" name="comment" id="comment" rows="5" placeholder="'.esc_attr__('Comments','appro').'" required="required" ></textarea></div>';        
        $default['title_reply'] = esc_html__( 'Leave a Reply' , 'appro' );
        $default['title_reply_to'] = esc_html__('Leave a Reply to ','appro').'%s';
        $default['title_reply_before'] = '<h4>';
        $default['title_reply_after'] = '</h4>';
        $default['cancel_reply_before'] = ' <small>';
        $default['cancel_reply_after'] = '</small>';
        $default['label_submit'] = esc_html__('Comment Submit','appro');
        $default['class_submit'] = 'bttn';
        $default['class_form'] = 'comment-form';
        $default['comment_notes_after'] = '<p>'.esc_html__('Allowed Tags: ','appro').'<code> ' . allowed_tags() . ' </code></p>';
        $default['comment_notes_before'] = esc_html__('Your email address will not be published.','appro');
        $default['logged_in_as'] = '<p class="logged-in-as">' . sprintf( esc_html__('Logged in as','appro').' <a href="%1$s"> %2$s </a><a href="%3$s" title="'.esc_attr__('Log out of this account','appro').'"> '.esc_html__('Log out?','appro').'</a>', admin_url( 'profile.php' ), $current_user->display_name, wp_logout_url( apply_filters( 'the_permalink', get_permalink( ) ) ) ) . '</p>';
        return $default;
    }
}
add_action('comment_form_defaults','appro_comment_form_df');
function appro_move_comment_field_to_bottom( $fields ) {
    $comment_field = $fields['comment'];
    unset( $fields['comment'] );
    $fields['comment'] = $comment_field;
    return $fields;
} 
add_filter( 'comment_form_fields', 'appro_move_comment_field_to_bottom' );
/*************************************
Comment form default fields
**************************************/
if( ! function_exists('appro_comment_form_df_fields') ){
    function appro_comment_form_df_fields(){
        $commenter = wp_get_current_commenter();
        $req = get_option('require_name_email');
        $aria_req = ( isset($req) ? 'required="required"' : '');
        $field = array(
            'author' => '<div class="form-double"><div class="form-group"><input type="text" name="author" placeholder="'. esc_attr__('Your Name','appro') .'" value="'.esc_attr($commenter['comment_author']).'" class="form-control" '.  $aria_req .' ></div>',
            'email' => '<div class="form-group last"><input type="email" name="email" value="'.esc_attr($commenter['comment_author_email']).'" placeholder="'. esc_attr__('Email Address','appro') .'" class="form-control" '. $aria_req .' ></div></div>',
            'url' => '<div class="form-group"><input name="url" value="'. esc_attr( $commenter['comment_author_url'] ) .'" class="form-control" placeholder="'. esc_attr__('Website URL','appro').'" type="url"></div>'
        );
        return $field;
    }
}
add_action('comment_form_default_fields','appro_comment_form_df_fields');