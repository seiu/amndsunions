<?php if( is_active_sidebar('appro_main_sidebar') ){ ?>
   <div class="hidden-xs col-sm-3 col-md-3 col-md-offset-1">
    <aside class="sidebar">
        <?php dynamic_sidebar('appro_main_sidebar'); ?>
    </aside>
</div>
<?php } ?>