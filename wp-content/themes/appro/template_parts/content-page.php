<?php
/**
 * Template part for displaying pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Appro
 * @since Appro 1.0
 */

?>
<?php
    the_content();
    wp_link_pages( 
        array(
            'before' => '<div class="page-links">',
            'after'  => '</div>',
            'next_or_number' => 'next',
            'nextpagelink'     => '<i class="fa fa-long-arrow-right"></i>',
            'previouspagelink' => '<i class="fa fa-long-arrow-left"></i>',
        )
    );