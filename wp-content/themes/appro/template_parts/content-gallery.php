<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Appro
 * @since Appro 1.0
 */

?><article <?php post_class( 'post-single'); ?> id="post_<?php the_ID(); ?>" >
        <?php if( ! is_single() ): ?>
        <header class="post-info">
            <?php
            the_title( '<h2 class="post-title" ><a href="' . get_permalink() . '" rel="bookmark">', '</a></h2>' );
            if(function_exists('appro_posted_on') && get_post_type() === 'post' ){
               appro_posted_on(); 
            }
        ?>
        </header>
        <?php endif; ?>
        <?php $gallery_images = get_post_meta( get_the_ID(), '_appro_post_gallery', 1 ); ?>
        <?php if(function_exists('appro_gallery_photo_list') && !empty($gallery_images) || has_post_thumbnail()) : ?>
        <div class="post-media">
            <?php
            if(function_exists('appro_gallery_photo_list') && !empty($gallery_images)){
                echo appro_gallery_photo_list($gallery_images);
            }elseif( function_exists('appro_post_thumbnail') ){
                appro_post_thumbnail();
            }
        ?>
        </div>
        <?php endif; ?>
        <div class="post-body">
            <?php
            if( is_single() ){
                the_content( sprintf(
                    wp_kses(
                        esc_html__( 'Continue reading', 'appro' ).'<span class="screen-reader-text"> "%s"</span>',
                        array(
                            'span' => array(
                                'class' => array(),
                            ),
                        )
                    ),
                    get_the_title()
                ) );
            }else{
                the_excerpt();
            }     
            wp_link_pages( array(
                'before' => '<div class="page-links">',
                'after'  => '</div>',
                'next_or_number' => 'next',
                'nextpagelink'     => '<i class="fa fa-long-arrow-right"></i>',
                'previouspagelink' => '<i class="fa fa-long-arrow-left"></i>',
            ) );
        if( ! is_single() ): ?>
            <?php
                if( function_exists('post_footer_content') ){
                    post_footer_content();
                }
            ?>
        <?php endif; ?>
        </div>
        <?php if(is_single()): ?>
        <div class="post-footer">
            <?php
                appro_entry_footer();
            ?>
        </div>
        <?php if(function_exists('appro_post_share_social')): ?>
        <div class="post-shares">
            <?php appro_post_share_social(); ?>
        </div>
        <?php endif; ?>
        <?php endif; ?>
    </article>