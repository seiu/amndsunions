<?php    
if( ! function_exists('appro_apus_get_load_plugins') ){
    function appro_apus_get_load_plugins() {        
        $plugins[] =(array(
            'name'               => esc_html__('King Composer','appro'),
            'slug'               => 'kingcomposer',
            'required'           => true
        ));
        
        $plugins[] =(array(
            'name'               => esc_html__('Smart Slider 3','appro'),
            'slug'               => 'smart-slider-3',
            'required'           => true
        ));
        
        $plugins[] =(array(
            'name'               => esc_html__('Appro Toolkit','appro'),
            'slug'               => 'appro-toolkit',
            'source'             => APPRO_ROOT .'/plugins/appro-toolkit.zip',
            'required'           => true,
            'version'            => '1.0.0'
        ));
        
        $plugins[] =(array(
            'name'               => esc_html__('Appro Share Post','appro'),
            'slug'               => 'appro-share-post',
            'source'             => APPRO_ROOT .'/plugins/appro-share-post.zip',
            'required'           => false,
            'version'            => '1.0'
        ));
        
        $plugins[] =(array(
            'name'               => esc_html__('CMB2','appro'),
            'slug'               => 'cmb2',
            'required'           => true
        ));
        
        $plugins[] =(array(
            'name'               => esc_html__('Tidio Live Chat','appro'),
            'slug'               => 'tidio-live-chat',
            'required'           => false
        ));
        
        $plugins[] =(array(
            'name'               => esc_html__('Breadcrumb NavXT','appro'),
            'slug'               => 'breadcrumb-navxt',
            'required'           => true, 
        ));
        
        $plugins[] =(array(
            'name'               => esc_html__('Mailchimp','appro'),
            'slug'               => 'mailchimp-for-wp',
            'required'           => false
        ));
        
        $plugins[] =(array(
            'name'               => esc_html__('Contact Form 7','appro'),
            'slug'               => 'contact-form-7',
            'required'           => false
        ));
        
        tgmpa( $plugins );
    }
}