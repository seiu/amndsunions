<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Appro
 * @since Appro 1.0
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 ====================================================================================================
 */
    /**********************
    *Comments List
    ***********************/
    if( !function_exists('appro_comments_list') ){
    function appro_comments_list($comment, $args, $depth){
    $GLOBALS['comment'] = $comment;
    /*Set avatar size*/
    $avatar_size = '100';
    if( $comment->comment_parent != '0'){
        $avatar_size = '80';
    }      
    if( have_comments() ){ ?>
    <?php if( get_comment_type() == 'pingback' || get_comment_type() == 'trackback' ): ?>
    <li id="commet-<?php comment_ID(); ?>">
        <article <?php comment_class( 'comment-item' ); ?> >
            <div class="comment-text">
                <div class="comment-header">
                    <h5><?php comment_author(); ?></h5>
                    <p class="comment-date"><?php esc_html_e( 'Posted on','appro' ); ?> <?php comment_date( get_option('date_format')); ?> <?php esc_html_e( 'at','appro' ); ?> <?php comment_time(get_option('time_formats')); ?> </p>
                    <?php if($comment->comment_approved == '0'){ ?>
                    <em><?php esc_html_e('Your comment is awaiting for moderator','appro'); ?></em>
                    <?php } ?>
                </div>
                <?php comment_text(); ?>
                <p><?php edit_comment_link(); ?></p>
            </div>
        </article>
        <?php endif; ?>
        <?php if( get_comment_type() == 'comment' ): ?>
        <li id="commet-<?php comment_ID(); ?>">
            <article <?php comment_class( 'comment-item'); ?> >
                <figure class="comment-pic">
                    <?php echo get_avatar( $comment->comment_author_email,$avatar_size); ?>
                </figure>
                <div class="comment-text">
                    <div class="comment-header">
                        <h5><?php if($comment->comment_author_url): ?> <a target="_blank" href="<?php echo esc_url($comment->comment_author_url); ?>"><?php comment_author(); ?></a><?php 
                            else:
                                comment_author();
                            endif;
                            ?></h5>
                        <p class="comment-date"><?php esc_html_e( 'Posted on','appro' ); ?><?php comment_date( get_option('date_format')); ?><?php esc_html_e( 'at','appro' ); ?><?php comment_time(get_option('time_formats')); ?> </p>
                        <?php if($comment->comment_approved == '0'){ ?>
                        <em><?php esc_html_e('Your comment is awaiting for moderator','appro'); ?></em>
                        <?php } 
                        ?></div>
                    <?php comment_text(); ?>
                    <?php
                          comment_reply_link(array_merge( $args, array('depth' => $depth) )); 
                    ?></div>
            </article>
            <?php endif; ?>
            <?php } } }
if ( post_password_required($post) ) {
    printf(wp_kses('<div class="password_protected">'.esc_html__('This post is password protected. Enter the password to view the comments.','appro').'</div>',wp_kses_allowed_html('post')));
    return;   
}
?><div id="comments" class="comments-area"><?php
        /*-- You can start editing here -- including this comment! --*/
        if ( have_comments() ) : ?>
                    <h4><?php 
                            $comment_count = get_comments_number();
                            if ( '1' === $comment_count ) {
                                printf(
                                    /* translators: 1: title. */
                                    esc_html__('One Comment on','appro').esc_html(' &ldquo;').'%1$s'.esc_html('&rdquo;'),
                                    '<span>' . get_the_title() . '</span>'
                                );

                            } else {
                                printf( /*WPCS: XSS OK.*/
                                    /* translators: 1: comment count number, 2: title. */
                                    '%1$s '.esc_html__('Comments on','appro').' '. esc_html('&ldquo;') .'%2$s'.esc_html('&rdquo;'),
                                    number_format_i18n( $comment_count ),
                                    '<span>' . get_the_title() . '</span>'
                                );
                            }
                        ?></h4>
                    <!-- .comments-title -->
                    <ul class="comments-list">
                        <?php wp_list_comments('callback=appro_comments_list'); ?>
                    </ul>
                    <!-- .comment-list -->
                    <div class="pagination">
                    <?php 
                        $paginet = array(
                            'prev_text' => '<i class="ti-arrow-left"></i>',
                            'next_text' => '<i class="ti-arrow-right"></i>',
                            'screen_reader_text' => ' ',
                            'type' => 'array',
                            'show_all' => true,
                        );
                        the_comments_pagination( $paginet );
                    ?>
                    </div>
                    <?php /* If comments are closed and there are comments, let's leave a little note, shall we?*/
                if ( ! comments_open() ) : ?>
                    <p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'appro' ); ?></p>
                <?php
                endif;
                endif; /*Check for have_comments().*/
                    comment_form();
                ?>
            </div>
            <!-- #comments -->