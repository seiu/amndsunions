<?php
/**
 * Extras functions for page builders
 *
 * @package Hestia
 * @since Hestia 1.1.24
 * @author Themeisle
 */

/**
 * Header for page builder blank template
 *
 * @since 1.1.24
 * @access public
 */
function hestia_no_content_get_header() {

	?>
	<!DOCTYPE html>
	<html <?php language_attributes(); ?> class="no-js">
	<head>
		<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-52GZNF');</script>
<!-- End Google Tag Manager -->
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta property="og:url"                content="http://americaneedsunions.org" />
<meta property="og:type"               content="website" />
<meta property="og:image"              content="http://americaneedsunions.org/wp-content/uploads/2018/05/ANU.png" />
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:site" content="@SEIU" />
<meta name="twitter:creator" content="@SEIU" />
<meta property="og:title" content="America Needs Union Jobs" />
<meta property="og:description" content="Stand Up With Working Families! Powerful billionaires are using the Supreme Court Case, Janus v. AFSCME to attack unions, divide working people, and further rig the economy against working families." />

		<link rel="profile" href="http://gmpg.org/xfn/11">
		<?php wp_head(); ?>

<link href="https://fonts.googleapis.com/css?family=Lato:400,400i,700,900" rel="stylesheet">
<style>
body { background-color: #ffffff; }
h1,h2,h3,h4,h5,h6, p, ol, ul, li { font-family: 'Lato', sans-serif !important; color: #333333; }
h1,h2,h3,h4,h5,h6 { font-weight: bold;  }
p, li { font-size: 1.3em; }
h1.site-title { text-align: center; }  
.at-title { font-size: 3em !important; }
label { font-size: 1.2em !important; }
.at input[type="text"], .at input[type="tel"], .at input[type="email"] { border-width: 1px !important;
    padding: 20px 10px !important; !important; font-size: 1.3em !important; }
.at fieldset legend { display:none; }
.section-title { color: #ffffff; }
.banner { background-image: url(http://americaneedsunions.org/wp-content/uploads/2018/05/ANUJ-Graphiccolor-overlaysmall.jpg);
text-align: center; }
.logo { margin-top: 3em; height: auto;
    width: 30em;
    margin-bottom: 2em; }
.blue { background-color: #3884e8; }
.blue p, .blue h4, .blue h5 { color: #ffffff; }
.left { float: left; }
.right { float: right; }
.left, .right { width: 46%; padding: 1%; }
.clear { clear: both; }
.container { max-width: 910px; }
footer.privacy { background-color: #D8D8D8; border: 1px solid #979797; text-align: center; padding: 6em; }
footer a { color: #333333; text-decoration: underline; }
button.host { background-color: #dc2026; font-family: 'Lato', sans-serif; }
ul.social-btns { margin: 2em 0; padding: 0;}
ul.social-btns li { display: inline-block; margin-right: 2rem; }
ul.social-btns li img { width: 60px; height: 60px; border: 2px solid #ffffff; border-radius: 18px; }
.host button { font-size: 3rem; font-weight: bold; }
.intro-text { font-size: 2.3rem; text-align: center; font-weight: bold; padding: 5rem; color: red; }
.pad { padding-top: 6rem; padding-bottom: 6rem; }
.host { background-image: url(http://americaneedsunions.org/wp-content/uploads/2018/05/AmericaNeedsUnionJobs-bkgd.jpg); }
.host h3, .host p { color: #ffffff; }
p.hostbtn { text-align:center; text-align:center; padding: 3rem 0 10rem; }
span.caption { font-size: small; color: #ffffff; }
.linkgroup { margin: 0 auto; text-align: center; font-size: 1.3em; padding: 0 5%; }

@media (max-width: 600px) {
	/*.banner { height: 22em; }*/
.at-title { font-size:1.3em; }
	p.hostbtn { padding: 3rem 0 0; }
	.pad { padding-top: 3rem;
    padding-bottom: 3rem; } 
}

@media (max-width: 400px) {
	.banner { height: 42em; }
}

</style>


		

		<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-54107421-22"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-54107421-22');
</script>

	</head>

	<body <?php body_class(); ?>>
		<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-52GZNF"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
	<?php
	do_action( 'hestia_page_builder_content_body_before' );

}

/**
 * Footer for page builder blank template
 *
 * @since 1.1.24
 * @access public
 */
function hestia_no_content_get_footer() {
	do_action( 'hestia_page_builder_content_body_after' );
	wp_footer();
	?>
	</body>
	</html>
	<?php
}


/**
 * Add header and footer support for beaver.
 *
 * @since 1.1.24
 * @access public
 */
function hestia_header_footer_render() {

	if ( ! class_exists( 'FLThemeBuilderLayoutData' ) ) {
		return;
	}

	// Get the header ID.
	$header_ids = FLThemeBuilderLayoutData::get_current_page_header_ids();

	// If we have a header, remove the theme header and hook in Theme Builder's.
	if ( ! empty( $header_ids ) ) {
		remove_action( 'hestia_do_header', 'hestia_the_header_content' );
		add_action( 'hestia_do_header', 'FLThemeBuilderLayoutRenderer::render_header' );
	}

	// Get the footer ID.
	$footer_ids = FLThemeBuilderLayoutData::get_current_page_footer_ids();

	// If we have a footer, remove the theme footer and hook in Theme Builder's.
	if ( ! empty( $footer_ids ) ) {
		remove_action( 'hestia_do_footer', 'hestia_the_footer_content' );
		add_action( 'hestia_do_footer', 'FLThemeBuilderLayoutRenderer::render_footer' );
	}
}
add_action( 'wp', 'hestia_header_footer_render' );

/**
 * Add theme support for header and footer.
 *
 * @since 1.1.24
 * @access public
 */
function hestia_header_footer_support() {
	add_theme_support( 'fl-theme-builder-headers' );
	add_theme_support( 'fl-theme-builder-footers' );
}
add_action( 'after_setup_theme', 'hestia_header_footer_support' );
